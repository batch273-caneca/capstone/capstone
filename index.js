// alert("Capstone Day");

class Customer {
	constructor(email) {
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut() {
		if(this.cart.isEmpty()) {
    console.log('cart is empty')
    	return;
		}
    this.cart.computeTotal()
    const order = {
    	products: this.cart.getContents(),
      	totalAmount:this.cart.totalAmount
    };
    this.orders.push(order);
    this.cart.clearCartContents();
    return this
	}

}

class Cart {
	constructor(contents, totalAmount) {
		this.contents = [];
		this.totalAmount = totalAmount;
	}

	addToCart(product, quantity) {
		this.contents.push({product, quantity});
    return this
	}

	showCartContents() {
		console.log(`${this.contents}`)
    return this
	}

	updateProductQuantity(name, newQuantity) {
		const updProd = this.contents.findIndex(i => i.product.name === name);
    if (updProd === -1) {
    return
    }
    this.contents[updProd]. quantity = newQuantity;
    return this
	}

	clearCartContents() {
		this.contents = [];
    this.totalAmount = 0
    return this;
	}

	computeTotal() {
		let total = 0;
    for (const item of this.contents) {
    total += item.product.price * item.quantity;
    }
    this.totalAmount = total;
 		return this
	}
  
  
  isEmpty() {
  return this.contents.length === 0 ;
  return this
  }
  
  getContents() {
  	return this.contents;
  }
}

	

class Product {
	constructor(name, price, isActive) {
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive() {
		if (this.isActive = true) {
			this.isActive = false;
		}
	}

	updatePrice(updPrice) {
		this.price = updPrice;
    	return this
	}
}